<?php

	// Afficher toutes les variables passées en GET
	var_dump($_POST);

	// Je récupère les valeurs du formulaire
	$nom = !empty($_POST ["nom"]) ? $_POST ["nom"] : '';
	$prenom = !empty($_POST ["prenom"]) ? $_POST ["prenom"] : '';
	$age = !empty($_POST ["age"]) ? $_POST ["age"] : '';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POST</title>
</head>
<body>

<?php
	
	if ($nom) {
		echo "Le nom saisi est : " . $nom . "<br />";
	}
	if ($prenom) {
		echo "Le prenom saisi est : " . $prenom . "<br />";
	}
	if ($age) {
		echo "L'age saisi est : " . $age . "<br />";
	}

?>

<form method="POST">
	<label for="nom">Nom d'utilisateur: </label>
	<input type="text" id= "nom" name="nom" />
	<label for="prenom">Prénom: </label>
	<input type="text" id = "prenom" name="prenom" />
	<label for="age">Age:</label>
	<input type="number" id = "age" name="age" />
<input type="submit" value="Submit">
</form>

    
</body>
</html>
