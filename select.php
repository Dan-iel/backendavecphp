<?php
    require 'connexion.php';

    // Requête pour récupère le film ayant un ID spécific
    $id = !empty($_GET['id']) ? (int) $_GET['id'] : NULL;

    if ($id) {
        $sql = 'SELECT * FROM movies WHERE id = ' . $id;
        $res = $mysqli->query($sql);
        $film = $res->fetch_assoc();
        if ($film) {
            echo "Le film ayant l'id " . $id . ' est : ' . $film['movie_name'] . '<br />';
        }
        else {
            echo "Aucun film trouvé pour l'id " . $id . '<br />';
        }
    }


    // Requête pour récupèrer tous les films
    $sql = 'SELECT * FROM movies';

    // J'envoie la requête à MySQL pour l'exécuter
    $res = $mysqli->query($sql);

    // Je récupère la liste des films retournée par MySQL
    $films = $res->fetch_all();

    // Je peux boucler dessus
    foreach ($films as $film) {
        echo 'Id: ' . $film[0] . '<br />';
        echo 'Titre: ' . $film[1] . '<br /><br />';
    }


    // Version plus simple
    $sql = 'SELECT * FROM movies';
    $res = $mysqli->query($sql);

    foreach ($res as $film) {
        echo 'Id: ' . $film['id'] . '<br />';
        echo 'Titre: ' . $film['movie_name'] . '<br /><br />';
    }

?>

