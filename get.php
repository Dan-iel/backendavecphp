<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Get</title>
</head>
<body>


	<?php 

		// Afficher toutes les variables passées en GET
		// var_dump($_GET);

		if (empty($_GET['id'])) {
			echo "Veuillez passer un id<br />";
			echo "Exemple url?id=X";
		}
		else {
			// On récupère l'ID, et on le transforme en entier
			$id = (int) $_GET['id'];
			echo "L'ID récupéré est : " . $id;
		}

	?>
    
</body>
</html>
